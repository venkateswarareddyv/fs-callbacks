const {readingFile,writeUpperCaseData,writeLowerCaseData,writeSortedData,readFileDeleteListedFiles} = require('../problem2.cjs');

readingFile((data)=>{
    writeUpperCaseData(data,()=>{
        writeLowerCaseData(()=>{
            writeSortedData(()=>{
                readFileDeleteListedFiles((message)=>{
                    console.log(message);
                });
            });
        });
    });
});

