const fs = require('fs');
const path = require('path');

function createDirectory(directoryname){
    let paths=path.join(__dirname,directoryname)
    fs.mkdir(paths,{recursive:true},(error)=>{
        if(error){
            throw Error("failed")
        }else{
            let x = Math.floor((Math.random() * 10));
            for(let i=0;i<x;i++){
                fs.writeFile(paths+`/filename${i}.JSON`,"dummy",(error)=>{
                    if(error){
                        throw Error("failed files")
                    }else{
                        fs.unlink(paths+`/filename${i}.JSON`,(error)=>{
                            if(error){
                                throw Error("failed delete")
                            }else{
                                console.log('files delete success')
                            }
                        })
                    }
                })
            }
        }
    })
}

module.exports =createDirectory;